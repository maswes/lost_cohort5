function [s_out,vco_n] = costas_pll(x_in,fs,fn,zeta)
% Implements a costas pll
%
% costas_pll(x,fs,fn,zeta);
% x: input complex vector
% fs: sample rate
% fn: natural frequency
% zeta: damping factor

    T = 1/fs;
    
    Kt=4*pi*zeta*fn;			% loop gain
    a=pi*fn/zeta;				% loop filter parameter
    
    f_in_last = 0; 
    f_int_out_last =0;
    vco_in_last = 0; 
    vco_out = 0; 
    vco_out_last = 0;
    
    npts = length(x_in);
        
    xI = zeros(1,npts);
    xQ = zeros(1,npts);   
    
    vco_n = zeros(1,npts);

    % Beginning of simulation loop
    for i=1:npts	
        
        % Generate De-Rotation Matrix
        %R = [ cos(-vco_out) -sin(-vco_out)  ;  
        %      sin(-vco_out)  cos(-vco_out) ];

        % input vector
        %x = [ real(x_in(i))  ;
        %      imag(x_in(i)) ];

        %y = R*x;  

        % phase de-rotator
        Rej_x = exp(-1i*vco_out) * x_in(i);        
                        
        % extract I and Q
        sI = real(Rej_x);                                    
        sQ = imag(Rej_x);

        xI(i) = sI;
        xQ(i) = sQ;
                 
        e1 = sI*sign(sQ);                      % LabVIEW phase detector "costas"
        e2 = sign(sI)*sQ;
        e = e2-e1;

        s3=Kt*e;                        % Gain
        f_in = s3;                      % Input to loop filter
        f_int_out = f_int_out_last + a*(T/2)*(f_in + f_in_last); 
        f_out = f_in + f_int_out;

        % Output of loop filter is last value pulse a discrete form of the 
        % integration of the input

        f_in_last = f_in;       % Update 
        vco_in = f_out;       
        f_int_out_last = f_int_out;

        % VCO input is sum of scaled output from phase detector 
        % and the integrating loop filter

        vco_out = vco_out_last + (T/2)*(vco_in + vco_in_last);

        % VCO integrates the phase - same form as integrator for 
        % the loop filter
%         if( i == 1)
%             vco_in_last = v_in;
%         else
            vco_in_last = vco_in;			% Update
%         end
        
        vco_out_last = vco_out;			% Update

        vco_n(i) = vco_out;
    end
    % End of simulation loop
    
    s_out = xI+1i*xQ;
    
%     v_out = vco_n(end)/2/pi;
end