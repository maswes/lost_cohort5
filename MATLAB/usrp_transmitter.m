clear all
close all
clc

%% usrp_transmitter -- LOST group USRP receiver script
% This script runs the LOST group USRP transmitter and is loosely based off 
% the MATLAB script sdruFrequencyCalibrationTransmitter.m

connectedRadios = findsdru;
if strncmp(connectedRadios(1).Status, 'Success', 7)
  radioFound = true;
  platform = connectedRadios(1).Platform;
  switch connectedRadios(1).Platform
    case {'B200','B210'}
      address = connectedRadios(1).SerialNum;
    case {'N200/N210/USRP2','X300','X310'}
      address = connectedRadios(1).IPAddress;
  end
else
  radioFound = false;
  address = '192.168.10.20';
  platform = 'N200/N210/USRP2';     
end

%% Initialization
% Set the properties of the SDRu transmitter, SDRU receiver, and the
% spectrum analyzer System object.

rfTxFreq = 445e6; % Nominal RF transmit center frequency
rfRxFreq           = 450e6;  % Nominal RF receive center frequency

prmFreqCalibTx = configureFreqCalibTx(platform, rfTxFreq);
prmFreqCalibRx = configureFreqCalibRx(platform, rfRxFreq);

% Sine source for troubleshooting. NOT USED in actual transmit
% hSineSource = dsp.SineWave (...
%     'Frequency',           prmFreqCalibTx.SineFrequency, ...
%     'Amplitude',           prmFreqCalibTx.SineAmplitude,...
%     'ComplexOutput',       prmFreqCalibTx.SineComplexOutput, ...
%     'SampleRate',          prmFreqCalibTx.Fs, ...
%     'SamplesPerFrame',     prmFreqCalibTx.SineFrameLength, ...
%     'OutputDataType',      prmFreqCalibTx.SineOutputDataType);

% The host computer communicates with the USRP(R) radio using the SDRu
% transmitter System object. B200 and B210 series USRP(R) radios are
% addressed using a serial number while USRP2, N200, N210, X300 and X310
% radios are addressed using an IP address. The parameter structure,
% prmFreqCalibTx, sets the CenterFrequency, Gain, and InterpolationFactor
% arguments.

% Set up radio tx object to use the found radio
switch platform
  case {'B200','B210'}
    radioTx = comm.SDRuTransmitter( ...
      'Platform',             platform, ...
      'SerialNum',            address, ...
      'MasterClockRate',      prmFreqCalibTx.MasterClockRate, ...
      'CenterFrequency',      prmFreqCalibTx.USRPTxCenterFrequency, ...
      'Gain',                 prmFreqCalibTx.USRPGain,...
      'InterpolationFactor',  prmFreqCalibTx.USRPInterpolationFactor)
  case {'X300','X310'}
    radioTx = comm.SDRuTransmitter( ...
      'Platform',             platform, ...
      'IPAddress',            address, ...
      'MasterClockRate',      prmFreqCalibTx.MasterClockRate, ...
      'CenterFrequency',      prmFreqCalibTx.USRPTxCenterFrequency, ...
      'Gain',                 prmFreqCalibTx.USRPGain,...
      'InterpolationFactor',  prmFreqCalibTx.USRPInterpolationFactor)
  case {'N200/N210/USRP2'}
    radioTx = comm.SDRuTransmitter( ...
      'Platform',             platform, ...
      'IPAddress',            address, ...
      'CenterFrequency',      prmFreqCalibTx.USRPTxCenterFrequency, ...
      'Gain',                 prmFreqCalibTx.USRPGain,...
      'InterpolationFactor',  prmFreqCalibTx.USRPInterpolationFactor, ...
      'ClockSource',           'External')
%       'ClockSource',           'Internal')
end

% Set up radio rx object to use the found radio
% A single radio can be full duplexed for rx AND tx, or set as only rx and
% tx
% switch platform
%   case {'B200','B210'}
%     radioRx = comm.SDRuReceiver(...
%         'Platform',         platform, ...
%         'SerialNum',        address, ...
%         'MasterClockRate',  prmFreqCalibRx.MasterClockRate, ...
%         'CenterFrequency',  prmFreqCalibRx.RxCenterFrequency,...
%         'Gain',             prmFreqCalibRx.Gain, ...
%         'DecimationFactor', prmFreqCalibRx.DecimationFactor,...
%         'SamplesPerFrame',  prmFreqCalibRx.FrameLength,...
%         'OutputDataType',   prmFreqCalibRx.OutputDataType)  
%   case {'X300','X310'}
%     radioRx = comm.SDRuReceiver(...
%         'Platform',         platform, ...
%         'IPAddress',        address, ...
%         'MasterClockRate',  prmFreqCalibRx.MasterClockRate, ...
%         'CenterFrequency',  prmFreqCalibRx.RxCenterFrequency,...
%         'Gain',             prmFreqCalibRx.Gain, ...
%         'DecimationFactor', prmFreqCalibRx.DecimationFactor,...
%         'SamplesPerFrame',  prmFreqCalibRx.FrameLength,...
%         'OutputDataType',   prmFreqCalibRx.OutputDataType)  
%   case {'N200/N210/USRP2'}
%     radioRx = comm.SDRuReceiver(...
%         'Platform',         platform, ...
%         'IPAddress',        address, ...
%         'CenterFrequency',  prmFreqCalibRx.RxCenterFrequency,...
%         'Gain',             prmFreqCalibRx.Gain, ...
%         'DecimationFactor', prmFreqCalibRx.DecimationFactor,...
%         'SamplesPerFrame',  prmFreqCalibRx.FrameLength,...
%         'OutputDataType',   prmFreqCalibRx.OutputDataType,...
%         'ClockSource',      'Internal')  
% %         'ClockSource',      'External')  
% end

% Set up MATLAB spectrum analyzer. Only used for troubleshooting
hSpectrumAnalyzer = dsp.SpectrumAnalyzer(...
    'Name',                          'Actual Frequency Offset',...
    'Title',                         'Actual Frequency Offset', ...
    'SpectrumType',                  'Power density',...
    'FrequencySpan',                 'Full', ...
    'SampleRate',                     prmFreqCalibRx.Fs, ...
    'YLimits',                        [-130,20],...
    'SpectralAverages',               50, ...
    'FrequencySpan',                  'Start and stop frequencies', ...
    'StartFrequency',                 -100e3, ...
    'StopFrequency',                  100e3,...
    'Position',                       figposition([50 30 30 40]));

% Load the spread waveform with canned text message
load('final_wf');

% Load canned PN sequence with no message. This is just a rand bit sequence
% of [-1,1]
load('canned_wf');

% Final wf
final_wf = (canwf-1/2)*2;

% Transform [0,1] to [1,-1]
asdf = final_wf .* -1; % 0 = 1, 1 = -1 in BPSK

% Waveform to be loaded into MATLAB radio object for tx
temp = asdf;

%% Stream Processing                   
%  Loop until the example reaches the target number of frames.

% Check for the status of the USRP(R) radio
if radioFound
%     for iFrame = 1: prmFreqCalibTx.TotalFrames1
    for iFrame = 1: 1e6 % Run for long time
        
        if exist('C:\LOST\tx.txt', 'file') == 2
            fid = fopen('C:\LOST\tx.txt', 'r');
%            pause(1);

            if fid > 1
                txmsg = fgetl(fid);
            end
            %pause(1);
%             txmsg = textread('C:\LOST\tx.txt', '%s');
            fclose('all');
            delete('C:\LOST\tx.txt');
        
            dec_in = ones(1, 30)*32; % msg is 30 bits; 32 is ascii ' '
            dec_in(1:length(txmsg)) = abs(txmsg); % coded message

            bin_in = dec2bin(dec_in,8)=='1'; % msg in binary
            txbin = bin_in';
            txbin = txbin(:); % tx message in binary
            txbpsk = (txbin-1/2)*2; % transform from [0,1] to [-1,1] bpsk
            txbpsk = txbpsk .* -1; % bpsk "0" = 1, "1" = -1, so flip signs
            % make sequence 3 bits long for redundancy (upsample)
            pattern=[];
            for k=1:length(txbpsk)
                if txbpsk(k)==-1
                    sig=ones(1,3).*-1;
                else
                    sig=ones(1,3);
                end
                pattern=[pattern sig];
            end
            txbpsk = pattern';
                   % preamble
            temp = [asdf; txbpsk];
            
            % We initally planned to spread the message with the preamble,
            % but the preamble ended up being spread with the message being
            % unspread and tacked on after the preamble.
        
% % Troubleshooting with sinewave:
%           sinewave =  step(hSineSource); % generate sine wave
%           step	; % transmit to USRP(R) radio

%           % Comment in/out to use radioTx
          step(radioTx, [temp; temp; temp]); % transmit to USRP(R) radio
          %step(radioTx, temp); % transmit to USRP(R) radio
        
          % For troubleshooting, you can full duplex mode the receiver,
          % radioRx, and see what you are receiving
%          [rxSig, len ] = step(radioRx);
%          if len > 0
%            % disp spec an
%            step(hSpectrumAnalyzer, rxSig);
%          end

            fclose('all');
            %delete('C:\LOST\tx.txt');

        else
            % do nothing if no tx.txt
            disp('no tx.txt file found') % can comment out later
        end
    end
    % Display the spectrum of what you sent after the simulation.
%     step(hSpectrumAnalyzer, temp);
else
    warning(message('sdru:sysobjdemos:MainLoop'))
end

%% Release System Objects
release (hSineSource);
release (radioTx);
release (radioRx);
release (hSpectrumAnalyzer);
clear radio
