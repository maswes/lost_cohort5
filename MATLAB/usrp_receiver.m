clear all
close all
clc

%% usrp_receiver -- LOST group USRP receiver script
% This script runs the LOST group USRP receiver and is loosely based off 
% the MATLAB script sdruFrequencyCalibrationReceiver.m

% whether or not rubidium is attached
rubidium_flag = 1;

connectedRadios = findsdru;
if strncmp(connectedRadios(1).Status, 'Success', 7)
  radioFound = true;
  platform = connectedRadios(1).Platform;
  switch connectedRadios(1).Platform
    case {'B200','B210'}
      address = connectedRadios(1).SerialNum;
    case {'N200/N210/USRP2','X300','X310'}
      address = connectedRadios(1).IPAddress;
  end
else
  radioFound = false;
  address = '192.168.10.20'; % sometimes you have to hard code the IP addr
  platform = 'N200/N210/USRP2';     
end

%% Initialization
% Set the properties of the SDRu transmitter, SDRU receiver, and the
% spectrum analyzer System object.

rfTxFreq = 445e6; % Nominal RF transmit center frequency
rfRxFreq = 445e6;  % Nominal RF receive center frequency

prmFreqCalibTx = configureFreqCalibTx(platform, rfTxFreq);
prmFreqCalibRx = configureFreqCalibRx(platform, rfRxFreq);

% Sine source for troubleshooting. NOT USED in actual transmit
% hSineSource = dsp.SineWave (...
%     'Frequency',           prmFreqCalibTx.SineFrequency, ...
%     'Amplitude',           prmFreqCalibTx.SineAmplitude,...
%     'ComplexOutput',       prmFreqCalibTx.SineComplexOutput, ...
%     'SampleRate',          prmFreqCalibTx.Fs, ...
%     'SamplesPerFrame',     prmFreqCalibTx.SineFrameLength, ...
%     'OutputDataType',      prmFreqCalibTx.SineOutputDataType);

% The host computer communicates with the USRP(R) radio using the SDRu
% transmitter System object. B200 and B210 series USRP(R) radios are
% addressed using a serial number while USRP2, N200, N210, X300 and X310
% radios are addressed using an IP address. The parameter structure,
% prmFreqCalibTx, sets the CenterFrequency, Gain, and InterpolationFactor
% arguments.

% You can run the radioTx in full duplex mode the same time as radioRx

% Set up radio tx object to use the found radio
% switch platform
%   case {'B200','B210'}
%     radioTx = comm.SDRuTransmitter( ...
%       'Platform',             platform, ...
%       'SerialNum',            address, ...
%       'MasterClockRate',      prmFreqCalibTx.MasterClockRate, ...
%       'CenterFrequency',      prmFreqCalibTx.USRPTxCenterFrequency, ...
%       'Gain',                 prmFreqCalibTx.USRPGain,...
%       'InterpolationFactor',  prmFreqCalibTx.USRPInterpolationFactor)
%   case {'X300','X310'}
%     radioTx = comm.SDRuTransmitter( ...
%       'Platform',             platform, ...
%       'IPAddress',            address, ...
%       'MasterClockRate',      prmFreqCalibTx.MasterClockRate, ...
%       'CenterFrequency',      prmFreqCalibTx.USRPTxCenterFrequency, ...
%       'Gain',                 prmFreqCalibTx.USRPGain,...
%       'InterpolationFactor',  prmFreqCalibTx.USRPInterpolationFactor)
%   case {'N200/N210/USRP2'}
%     radioTx = comm.SDRuTransmitter( ...
%       'Platform',             platform, ...
%       'IPAddress',            address, ...
%       'CenterFrequency',      prmFreqCalibTx.USRPTxCenterFrequency, ...
%       'Gain',                 prmFreqCalibTx.USRPGain,...
%       'InterpolationFactor',  prmFreqCalibTx.USRPInterpolationFactor, ...
%       'ClockSource',           'External')
% end

% Set up radio rx object to use the found radio
% A single radio can be full duplexed for rx AND tx, or set as only rx and
% tx
switch platform
  case {'B200','B210'}
    radioRx = comm.SDRuReceiver(...
        'Platform',         platform, ...
        'SerialNum',        address, ...
        'MasterClockRate',  prmFreqCalibRx.MasterClockRate, ...
        'CenterFrequency',  prmFreqCalibRx.RxCenterFrequency,...
        'Gain',             prmFreqCalibRx.Gain, ...
        'DecimationFactor', prmFreqCalibRx.DecimationFactor,...
        'SamplesPerFrame',  prmFreqCalibRx.FrameLength,...
        'OutputDataType',   prmFreqCalibRx.OutputDataType)  
  case {'X300','X310'}
    radioRx = comm.SDRuReceiver(...
        'Platform',         platform, ...
        'IPAddress',        address, ...
        'MasterClockRate',  prmFreqCalibRx.MasterClockRate, ...
        'CenterFrequency',  prmFreqCalibRx.RxCenterFrequency,...
        'Gain',             prmFreqCalibRx.Gain, ...
        'DecimationFactor', prmFreqCalibRx.DecimationFactor,...
        'SamplesPerFrame',  prmFreqCalibRx.FrameLength,...
        'OutputDataType',   prmFreqCalibRx.OutputDataType)  
  case {'N200/N210/USRP2'}
    radioRx = comm.SDRuReceiver(...
        'Platform',         platform, ...
        'IPAddress',        '192.168.10.22', ...
        'CenterFrequency',  prmFreqCalibRx.RxCenterFrequency,...
        'Gain',             prmFreqCalibRx.Gain, ...
        'DecimationFactor', prmFreqCalibRx.DecimationFactor,...
        'SamplesPerFrame',  prmFreqCalibRx.FrameLength,...
        'OutputDataType',   prmFreqCalibRx.OutputDataType,...
        'ClockSource',      'External')  
end

% Set up MATLAB spectrum analyzer. Only used for troubleshooting
hSpectrumAnalyzer = dsp.SpectrumAnalyzer(...
    'Name',                          'Actual Frequency Offset',...
    'Title',                         'Actual Frequency Offset', ...
    'SpectrumType',                  'Power density',...
    'FrequencySpan',                 'Full', ...
    'SampleRate',                     prmFreqCalibRx.Fs, ...
    'YLimits',                        [-130,20],...
    'SpectralAverages',               50, ...
    'FrequencySpan',                  'Start and stop frequencies', ...
    'StartFrequency',                 -100e3, ...
    'StopFrequency',                  100e3,...
    'Position',                       figposition([50 30 30 40]));

% Load the spread waveform with canned text message
load('final_wf');

% Load canned PN sequence with no message. This is just a rand bit sequence
% of [-1,1] generated beforehand
load('canned_wf');

% Final wf
final_wf = (canwf-1/2)*2;

% Transform [0,1] to [1,-1]
asdf = final_wf .* -1; % 0 = 1, 1 = -1 in BPSK

% Waveform to be loaded into MATLAB radio object for tx
temp = asdf;

% gardner symbol sync
symsync = comm.SymbolSynchronizer;
% matlab AGC object
agc = comm.AGC;
    
%% Stream Processing                   
%  Loop until the example reaches the target number of frames.

rx_ctr = 0; % counter for # of receives
rx_correct = 0; % counter for # of correct decodes, used for troubleshooting

% Check for the status of the USRP(R) radio
if radioFound
%     for iFrame = 1: prmFreqCalibTx.TotalFrames
    for iFrame = 1: 1e6 % Run for long time
        
%       % Comment in/out to use radioRx
        [rxSig, len ] = step(radioRx);
        if len > 0
    
            % Some random errors would stop simulation. Try/catch would
            % just continue simulation through errors. 
            % should probably NOT have this, but sometimes didn't have time
            % to debug
            try 
            rx_ctr = rx_ctr + 1;

%             thresh = rxSig > 0; 


        if rubidium_flag
            thresh = rxSig > 0; % threshold the data to determine 0/1   
            
            % time align frames to find peak of reference versus sampled
            % frame
            [corrchk lags] = xcorr((thresh-1/2)*2, final_wf); % correlate
            [mx ind] = max(abs(corrchk)); 
            newthresh = rxSig > 0;
        else
               
            h = rcosdesign(0.5, 2, 2); % LPF, probably not needed
            filtout = filter(h, 1, rxSig);
            
            [rxSym, tErr] = symsync(filtout); % gardner TED
            
            pllout = pll(rxSym, 200e3); % costas loop
            
%             [pllout v_out] = costas_pll(rxSym, 200e3,2,2);

%             newthresh = rxSym > 0; % threshold the data to determine 0/1
%             [corrchk lags] = xcorr((newthresh-1/2)*2, final_wf); % correlate
%             [mx ind] = max(abs(corrchk)); 
%             figure(1); plot(lags, abs(corrchk));
                        
            newthresh = pllout > 0; % threshold the data to determine 0/1
            
            [corrchk lags] = xcorr((newthresh-1/2)*2, final_wf); % correlate
            [mx ind] = max(abs(corrchk));             
            
%             figure(2); plot(lags, abs(corrchk));
        end
% 4x oversample -> pseudo ambiguity function
%             [corrchk1 lags1] = xcorr((thresh(1:4:end)-1/2)*2, final_wf); % correlate
%             [mx1 ind1] = max(abs(corrchk1)); 
% 
%             [corrchk2 lags2] = xcorr((thresh(2:4:end)-1/2)*2, final_wf); % correlate
%             [mx2 ind2] = max(abs(corrchk2)); 
% 
%             [corrchk3 lags3] = xcorr((thresh(3:4:end)-1/2)*2, final_wf); % correlate
%             [mx3 ind3] = max(abs(corrchk3)); 
% 
%             [corrchk4 lags4] = xcorr((thresh(4:4:end)-1/2)*2, final_wf); % correlate
%             [mx4 ind4] = max(abs(corrchk4)); 
%             
%             [maxi in] = max([mx1 mx2 mx3 mx4]);
%             
%             eval(['ind = ind' num2str(in) ';']);
%             eval(['newthresh = thresh(' num2str(in) ':4:end);']);

% 10 x oversample
%             [corrchk1 lags1] = xcorr((thresh(1:10:end)-1/2)*2, final_wf); % correlate
%             [mx1 ind1] = max(abs(corrchk1)); 
% 
%             [corrchk2 lags2] = xcorr((thresh(2:10:end)-1/2)*2, final_wf); % correlate
%             [mx2 ind2] = max(abs(corrchk2)); 
% 
%             [corrchk3 lags3] = xcorr((thresh(3:10:end)-1/2)*2, final_wf); % correlate
%             [mx3 ind3] = max(abs(corrchk3)); 
% 
%             [corrchk4 lags4] = xcorr((thresh(4:10:end)-1/2)*2, final_wf); % correlate
%             [mx4 ind4] = max(abs(corrchk4)); 
% 
%             [corrchk5 lags5] = xcorr((thresh(5:10:end)-1/2)*2, final_wf); % correlate
%             [mx5 ind5] = max(abs(corrchk5)); 
% 
%             [corrchk6 lags6] = xcorr((thresh(6:10:end)-1/2)*2, final_wf); % correlate
%             [mx6 ind6] = max(abs(corrchk6)); 
% 
%             [corrchk7 lags7] = xcorr((thresh(7:10:end)-1/2)*2, final_wf); % correlate
%             [mx7 ind7] = max(abs(corrchk7)); 
% 
%             [corrchk8 lags8] = xcorr((thresh(8:10:end)-1/2)*2, final_wf); % correlate
%             [mx8 ind8] = max(abs(corrchk8)); 
% 
%             [corrchk9 lags9] = xcorr((thresh(9:10:end)-1/2)*2, final_wf); % correlate
%             [mx9 ind9] = max(abs(corrchk9)); 
% 
%             [corrchk10 lags10] = xcorr((thresh(10:10:end)-1/2)*2, final_wf); % correlate
%             [mx10 ind10] = max(abs(corrchk10)); 

%             [maxi in] = max([mx1 mx2 mx3 mx4 mx5 mx6 mx7 mx8 mx9 mx10]);
%             
%             eval(['ind = ind' num2str(in) ';']);
%             eval(['newthresh = thresh(' num2str(in) ':10:end);']);

%             if ind < length(thresh) % corr is lagged        
%                 decodedRx = thresh((length(thresh)-1)-ind:(length(thresh)-1)-ind+length(final_wf)-1); % take peak of xcorr
%             else
%                 decodedRx = thresh(ind-(length(thresh)-1):ind-(length(thresh)-1)+length(final_wf)-1); % take peak of xcorr
%             end

            % Decoding using correlation peaks
            if ind < length(newthresh) % find # of samples correlation is off
                decodedRx = newthresh((length(newthresh)-1)-ind:(length(newthresh)-1)-ind+length(final_wf)-1); % take peak of xcorr
            else
                decodedRx = newthresh(ind-(length(newthresh)-1):ind-(length(newthresh)-1)+length(final_wf)-1); % take peak of xcorr
            end

            % Initialization vector
            decodedown = zeros(length(decodedRx)/3, 1);

            % Down sample from 3:1 bc tx is upsampled 1:3. 
            for kkk = 1:3:length(decodedRx) % "majority rules" 2/3 decoding
               temp2 = decodedRx(kkk:kkk+2);
               decodedown(ceil(kkk/3)) = sum(temp2) > 1;
            end
            final_wf_dig = (final_wf+1)/2; % digital [-1,1] to [0,1]

            % check bit differences. Also check the bit flip (~decodedown)
            chk1 = sum(abs(decodedown - final_wf_dig(1:3:end)));
            chk2 = sum(abs(~decodedown - final_wf_dig(1:3:end)));            
            
            % We initially didn't know why bits were flipped, then realized it was phase offset 
            if chk1 == 0 || chk2 == 0
                rx_correct = rx_correct + 1;
                rx_correct
                
                if ind < length(newthresh) % find # of samples correlation is off
                    rxmsg = newthresh((length(newthresh)-1)-ind+length(final_wf):(length(newthresh)-1)-ind+length(final_wf) + 30*8*3-1); % take data after preamble
                else
                    rxmsg = newthresh(ind-(length(newthresh)-1)+length(final_wf):ind-(length(newthresh)-1)+length(final_wf) + 30*8*3-1); % take data after preamble
                end
                % initialize vector
                rxmsgdown = zeros(length(rxmsg)/3, 1);
                
                for kkk = 1:3:length(rxmsg) % "majority rules" 2/3 decoding
                    temp3 = rxmsg(kkk:kkk+2);
                    rxmsgdown(ceil(kkk/3)) = sum(temp3) > 1;
                end
                        
                if chk1 == 0
                    % do nothing, msg should be fine
                elseif chk2 == 0
                    rxmsgdown = ~rxmsgdown; % bit flipped
                end
                
                fliprxmsgdown = ~rxmsgdown;
                
                rxreshaped = logical(reshape(rxmsgdown, [8 length(rxmsgdown)/8 ]));
                rxreshaped2 = logical(reshape(fliprxmsgdown, [8 length(fliprxmsgdown)/8]));
                
                temp4 = bin2dec(num2str(rxreshaped')); % temp string to convert to decimals
                temp5 = bin2dec(num2str(rxreshaped2')); % temp string to converet to decimals
                
                decoded_str = native2unicode(temp4', 'ascii');
                decoded_str2 = native2unicode(temp5', 'ascii'); 
                
                fid = fopen('C:\LOST\rx.txt', 'w');
                fwrite(fid, decoded_str);
                pause(0.2);
                fclose(fid);
                
            end
            % For troubleshooting
            step(hSpectrumAnalyzer, rxSig);
            catch
            end
        end
    end
    % Display the spectrum after the simulation.
%     step(hSpectrumAnalyzer, temp);
else
    warning(message('sdru:sysobjdemos:MainLoop'))
end

%% Release System Objects
%release (hSineSource);
%release (radioTx);
release (radioRx);
release (hSpectrumAnalyzer);
clear radio
