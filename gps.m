clear all; close all; clc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GPS v1                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Initialize %%%
gpsdata = 1;
rate  = 9600;
%%% Initialize %%%

% Open the serial port
s = serial('COM38', 'BaudRate', rate);
fopen(s);

while(gpsdata)
    data = fscanf(s);
    % Get Lat/Long/Time
    if(contains(data, 'GNGLL'))
        gpsdata_split = split(data, ',');
        gpsdata = 0;
    end
end

% Close the serial port
fclose(s);
delete(instrfindall);

lat  = gpsdata_split{2};                                    %   ddmm.mmmm
lat  = str2double(lat(1:2)) + str2double(lat(3:end)) / 60;
if(contains(gpsdata_split{3}, 'S'))
    lat = -lat;
end

long = gpsdata_split{4};                                    %  dddmm.mmmm
long = str2double(long(1:3)) + str2double(long(4:end)) / 60;
if(contains(gpsdata_split{5}, 'W'))
    long = -long;
end

utc  = gpsdata_split{6};                                    % hhmmss.sss
utc  = strcat(utc(1:2), ':', utc(3:4), ':', utc(5:end));
