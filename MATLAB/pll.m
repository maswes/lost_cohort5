function [y, p] = pll(x, fs)

% pll - Perfect second-order phase-locked loop.
%   Inputs - 
%     x  - Complex input
%    fs  - Sample rate (Hz)
%  coded - Bits to compare PLL output to
%   Outputs - 
%     y - PLL voltage output. Soft bit decisions to input to Turbo decoding
%     p - PLL phase output
 
PLOT_FLAG = 0;

% t = [0:1:(384+190-1)]'*1/640;
% x = exp(1i*2*pi*t*0.4);

%% Initialize paramters
fn   = 2; % Loop natural frequency (Hz)
zeta = 2; % Typical loop damping value
% fn   = 1; % Loop natural frequency (Hz)
% zeta = 1; % Typical loop damping value

dt = 1/fs; % Symbol period (s)

Kt = 4*pi*zeta*fn; % Loop gain
a  = pi*fn/zeta;   % Loop filter parameter

N = length(x);

% x2 = x.^2; % Remove BPSK bit flips by squaring
x2 = abs(x).*exp(1i*angle(x)*2); % Remove BPSK bit flips by doubling phase without squaring (same result as with squaring, but may be easier for hardware implementation)

%% PLL
f = zeros(N,1);
p = zeros(N,1);

vco_out       = 0;
filt_in_last  = 0; % Trapezoidal approximation integration value
filt_out_last = 0;
vco_in_last   = 0; % Trapezoidal approximation integration value
vco_out_last  = 0; 
for k = 1:1:N
    s1 = angle(x2(k)) - vco_out;
    s2 = sin(s1);
    s3 = Kt*s2;
    
    %% Loop filter, F(s)
    filt_in = a*s3;
    
    filt_out = filt_out_last + (filt_in + filt_in_last)/2*dt; % a/s (integration performed with trapezoidal rule)
    
    vco_in = s3 + filt_out; % F(s) = 1 + a/s
    
    %% VCO
    vco_out = vco_out_last + (vco_in + vco_in_last)/2*dt; % Integration (integration performed with trapezoidal rule)
    
    %% Update values
    filt_in_last  = filt_in;
    filt_out_last = filt_out;
    vco_in_last   = vco_in;
    vco_out_last  = vco_out;
    
    p(k) = vco_out;
    f(k) = vco_in/(2*pi); 
end

%% Demodulate output
y = x.*exp(-1i*p/2); % Remove excess frequency shift. Division by 2 because phase was doubled
% % % y = x.*exp(-1i*2*pi*mean(f)/2*[0:1:length(f)-1]'*dt);
y = real(y);   % Bit modulation

%% Plot
if(PLOT_FLAG)
    figure(201);
    clf;
    % plot(demoded_signal,'b.-');
    hold on;
    plot((diff(unwrap(angle(x2))/(2*pi))/dt)/2,'k.-');
    plot(f/2,'r.-');
    % plot(p*180/pi,'g.-');
    xlabel('Symbol');
    ylabel('Amplitude');
    grid on;
    % legend({'Demodulated Output','Frequency (Hz)','Phase (rot)','Error signal'},'location','southwest');
    % legend({'Frequency (Hz)','Phase (deg)'},'location','northwest');
    legend({'Actual Frequency (Hz)','PLL Frequency (Hz)'},'location','northwest');
    
%     figure(202);
%     clf;
%     hold on;
%     plot(coded,'b.-');
%     plot(sign(real(y)),'r.--');
%     xlabel('Symbol');
%     ylabel('Amplitude');
%     grid on;
%     title('Actual vs. Output');
%     legend({'Actual','Output'},'location','northwest');
end

% % %% Simulate input
% % t = [0:1:383]'*dt;
% % x = exp(1i*2*pi*t*1);


