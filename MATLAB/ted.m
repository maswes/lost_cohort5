function [ xx ] = ted( x_in, h_mf, N )
%TED: zc-ted / gardner ted

    L = length(x_in);    
    
    G = 10;
    K1 = -2.4609e-3 * G;
    K2 = -8.2030e-6 * G;
    
    x_filt = conv(x_in,h_mf);
    Lf = length(h_mf);
    Lx = length(x_in);
    Dg = (Lf-1)/2;      % group delay
    
    x_filt = x_filt( (1 + Dg) : Lx - Dg );
    x = x_filt(1:(N/2):end);
    
    CNT_next = 0;
    mu_next = 0;
    underflow = 0;
    vi = 0;
    TEDBuff = zeros(1,2);
    k = 1;
    
    a2 = [1, -1, -1, 1];
    a1 = [-1, 3, -1, -1];
    
    Lexp = length(x)-3;       % expected length is L / decimation factor.
    xx = zeros(1,Lexp);
    for n=2:length(x)-2
        
        CNT = CNT_next;
        mu = mu_next;
        
        % farrow filter interpolator
        v2 = 0.5*sum(a2 .* x(n+2:-1:n-1));
        v1 = 0.5*sum(a1 .* x(n+2:-1:n-1));
        v0 = x(n);        
        xI = (mu*v2 + v1)*mu + v0;
        
        if( underflow == 1 )
            eI = real(TEDBuff(1)) * (sign(real(TEDBuff(2)))-sign(real(xI)));
            eQ = imag(TEDBuff(1)) * (sign(imag(TEDBuff(2)))-sign(imag(xI)));
            
            % gardner
%             eI = real(TEDBuff(1)) * ((real(TEDBuff(2)))-(real(xI)));
%             eQ = imag(TEDBuff(1)) * ((imag(TEDBuff(2)))-(imag(xI)));
                     
            e = eI + eQ;
            
            xx(k) = xI;
            k = k + 1;
        else
            e = 0;
        end

        % pi filter
        vp = K1*e;
        vi = vi + K2*e;
        v = vp + vi;
        W = 1/2 + v;
        
        % update registers
        
        CNT_next = CNT - W;
        if( CNT_next < 0 )
            CNT_next = 1 + CNT_next;
            underflow = 1;
            mu_next = CNT/W;
        else
            underflow = 0;
            mu_next = mu;
        end
        
        %TEDBuff = [xI; TEDBuff(1)];
        TEDBuff(2) = TEDBuff(1);
        TEDBuff(1) = xI;
    end
    
    xx = xx(1:k);
end

